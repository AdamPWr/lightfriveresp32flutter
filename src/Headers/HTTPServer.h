#pragma once
#include <esp_http_server.h>

class HTTPServer
{

    static const char* TAG;
    httpd_uri_t uri_get;
    httpd_uri_t uri_post;
    httpd_handle_t server;
    
    
    public:

    HTTPServer();
    ~HTTPServer();
    esp_err_t start();
    esp_err_t stop();
    esp_err_t registerURI(httpd_uri_t uri);
    esp_err_t runProcedure();
    //esp_err_t get_handler(httpd_req_t *req);
    // esp_err_t post_handler(httpd_req_t *req);

};