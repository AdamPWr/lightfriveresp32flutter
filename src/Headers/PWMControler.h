#pragma once
#include"driver/ledc.h"
#include <math.h>
#include<vector>

class PWMControler
{

    ledc_timer_config_t* timerConf; // consider delete right after initialization
    ledc_channel_config_t* channelConf; // consider delete right after initialization
    static const int MAX_DUTY_RESOLUTION = std::pow(2, (int)LEDC_TIMER_13_BIT);
    std::vector<u_int16_t> currentChannelsDuty;
    std::vector<u_int16_t> channelsPinout;
    bool is_fade_func_installed = false;

    public:
    PWMControler(std::vector<uint16_t> p_channelsPinout);
    bool confPWM();
    // Function adds factor to CURRENT duty cycle
    bool changeDutyCycleOnPin(uint16_t pinNumber, int16_t factor);
    bool changeDutyCycleOnAllPins(int16_t factor);
    bool setMaxDutyOnPin(uint16_t pinNumber);
    bool setMaxDutyOnAllPins();
    bool setZeroDutyOnPin(u_int16_t pinNumber);
    bool setZeroDutyOnAllPins();
    // If a fade operation is running in progress on that channel,
    // the driver would not allow it to be stopped. Other duty operations will have to wait until the fade operation has finished. 
    // bool fadeWithTime();
    bool fadeWithTimeOnPin(uint32_t target_duty, uint32_t max_fade_time_ms, u_int16_t pinNumber);
    bool fadeWithTimeOnAllPins(uint32_t target_duty, uint32_t max_fade_time_ms);
    bool setDutyCycleOnPin(u_int8_t pinNumber, u_int8_t procentFactor);
    bool setDutyCycleOnAllPins(u_int8_t procentFactor);
    bool installFadeFunc();
    void uninstallFadeFunc();
    bool setFadeWithTimeByProcentOnPin(u_int8_t pinNumber, u_int8_t procentFactor, uint32_t max_fade_time_ms);
    bool setFadeWithTimeByProcentOnAllPins(u_int8_t procentFactor, uint32_t max_fade_time_ms);
    ~PWMControler();

    private:
    bool setPWMTimerConf();
    bool setPWMChannelsConfig();
    int16_t getChannelNumber(u_int16_t pinNumber);
};