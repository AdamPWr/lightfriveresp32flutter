#pragma once
#include<vector>
#include <stdint.h>

class MotionSensorControler
{
    std::vector<uint8_t> pir_pins;

public:
    MotionSensorControler(std::vector<uint8_t> pir_pins);

private:

    void setInteruptOnPin(uint8_t pinNumber);
};