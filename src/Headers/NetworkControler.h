#pragma once
#include<string>
#include "esp_event.h"

class NetworkControler
{
public:
    static const std::string TAG;
    const std::string EXAMPLE_ESP_WIFI_SSID = "aaa";
    const std::string EXAMPLE_ESP_WIFI_PASS = "12345678";
    const u_int8_t EXAMPLE_ESP_WIFI_CHANNEL = 3;
    const u_int8_t  EXAMPLE_MAX_STA_CONN = 3;



    static void wifi_event_handler(void* arg, esp_event_base_t event_base,
                                    int32_t event_id, void* event_data);


    void initAndStartWifiAsAP();
};
