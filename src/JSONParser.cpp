#include"Headers/JSONParser.h"
#include"cJSON.h"

void JSONParser::parse(std::string raw_content)
{
    cJSON *elem;
    cJSON *r;
    cJSON *g;
    cJSON *b;
    cJSON *root = cJSON_Parse(raw_content.c_str());
    int num_of_transitions = cJSON_GetArraySize(root);
    for (u_int8_t i = 0; i < num_of_transitions; i++) 
    {
        elem = cJSON_GetArrayItem(root, i);
        r = cJSON_GetObjectItem(elem, "r");
        g = cJSON_GetObjectItem(elem, "g");
        b = cJSON_GetObjectItem(elem, "b");
        printf("%s\n", r->valuestring);
        printf("%s\n", g->valuestring);
        printf("%s\n", b->valuestring);
    }
}