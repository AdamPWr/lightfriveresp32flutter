#include"../Headers/NetworkControler.h"

#include "lwip/err.h"
#include "lwip/sys.h"
#include "esp_system.h"
#include "esp_wifi.h"
#include "esp_log.h"
#include "nvs_flash.h"
#include <string.h>

const std::string NetworkControler::TAG = "\nwifi softAP";


void NetworkControler::wifi_event_handler(void* arg, esp_event_base_t event_base,
                                    int32_t event_id, void* event_data)
{
    if (event_id == WIFI_EVENT_AP_STACONNECTED) {
        wifi_event_ap_staconnected_t* event = (wifi_event_ap_staconnected_t*) event_data;
        printf("%s station with MAC %02x:%02x:%02x:%02x:%02x:%02x JOINED, AID %d\n",TAG.c_str(), MAC2STR(event->mac), event->aid);
    } else if (event_id == WIFI_EVENT_AP_STADISCONNECTED) {
        wifi_event_ap_stadisconnected_t* event = (wifi_event_ap_stadisconnected_t*) event_data;
        printf("%s station with MAC %02x:%02x:%02x:%02x:%02x:%02x LEAVED, AID %d\n",TAG.c_str(), MAC2STR(event->mac), event->aid);
    }
}

void NetworkControler::initAndStartWifiAsAP()
{

    ESP_ERROR_CHECK(nvs_flash_init());
    ESP_ERROR_CHECK(esp_netif_init());
    ESP_ERROR_CHECK(esp_event_loop_create_default());
    esp_netif_create_default_wifi_ap();

    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK(esp_wifi_init(&cfg));

    ESP_ERROR_CHECK(esp_event_handler_instance_register(WIFI_EVENT,
                                                        ESP_EVENT_ANY_ID,
                                                        wifi_event_handler,//&wifi_event_handler,
                                                        NULL,
                                                        NULL));

    wifi_config_t wifi_config = {};
    
    if ((u_int8_t)EXAMPLE_ESP_WIFI_PASS.length() == 0) {
        wifi_config.ap.authmode = WIFI_AUTH_OPEN;
    }
    memcpy(wifi_config.ap.ssid,EXAMPLE_ESP_WIFI_SSID.c_str(),sizeof(wifi_config.ap.ssid));
    memcpy(wifi_config.ap.password,EXAMPLE_ESP_WIFI_PASS.c_str(),sizeof(wifi_config.ap.password));
    wifi_config.ap.authmode = WIFI_AUTH_WPA_WPA2_PSK;
    wifi_config.ap.ssid_len = (u_int8_t)EXAMPLE_ESP_WIFI_SSID.length();
    wifi_config.ap.channel = EXAMPLE_ESP_WIFI_CHANNEL;
    wifi_config.ap.max_connection = EXAMPLE_MAX_STA_CONN;
    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_AP));
    ESP_ERROR_CHECK(esp_wifi_set_config(WIFI_IF_AP, &wifi_config));
    ESP_ERROR_CHECK(esp_wifi_start());

    printf("%s wifi_init_softap finished. SSID:%s password:%s channel:%d \n", TAG.c_str(),
    EXAMPLE_ESP_WIFI_SSID.c_str(),EXAMPLE_ESP_WIFI_PASS.c_str(), EXAMPLE_ESP_WIFI_CHANNEL);
    // ESP_LOGI(TAG.c_str(), "wifi_init_softap finished. SSID:%s password:%s channel:%d",
    //          EXAMPLE_ESP_WIFI_SSID.c_str(), EXAMPLE_ESP_WIFI_PASS.c_str(), EXAMPLE_ESP_WIFI_CHANNEL);
}
