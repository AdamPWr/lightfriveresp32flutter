#include"../Headers/HTTPServer.h"
#include <algorithm>
#include <sstream> 
#include"Headers/JSONParser.h"

const char* HTTPServer::TAG = "\nHTTPServer";

esp_err_t get_handler(httpd_req_t *req)
{
    printf("\n get");
    return ESP_OK;
}

esp_err_t post_handler(httpd_req_t *req)
{
    printf("\n post");

     /* Destination buffer for content of HTTP POST request.
     * httpd_req_recv() accepts char* only, but content could
     * as well be any binary data (needs type casting).
     * In case of string data, null termination will be absent, and
     * content length would give length of string */
    char content[100];
    int ret, remaining = req->content_len;
    std::stringstream ss;

    while (remaining > 0) {
        /* Read the data for the request */
        if ((ret = httpd_req_recv(req, content,
                        std::min((unsigned int)remaining, sizeof(content)))) <= 0) 
        {
            if (ret == HTTPD_SOCK_ERR_TIMEOUT) {
                /* Retry receiving if timeout occurred */
                continue;
            }
            return ESP_FAIL;
        }

        /* Send back the same data */
        //httpd_resp_send_chunk(req, buf, ret);
        remaining -= ret;
        ss << content;
    }

    /* Send a simple response */
    printf("content \n%s", ss.str().c_str());
    JSONParser::parse(ss.str());
    const char resp[] = "Transition received";
    httpd_resp_send(req, resp, HTTPD_RESP_USE_STRLEN);
    return ESP_OK;
}

HTTPServer::HTTPServer()
{
    uri_get.uri = "/";
    uri_get.handler = get_handler;
    uri_get.method = HTTP_GET;
    uri_get.user_ctx = nullptr;

    uri_post.uri = "/";
    uri_post.handler = post_handler;
    uri_post.method = HTTP_POST;
    uri_post.user_ctx = nullptr;
}

esp_err_t HTTPServer::registerURI(httpd_uri_t uri)
{
    if(server)
    {
        auto result = httpd_register_uri_handler(server,&uri);
        if(result == ESP_OK)
        {
            printf("%s URI %s registered", TAG, uri.uri);
        }
        else
        {
            printf("%s URI %s NOT registered", TAG, uri.uri);
        }
        return result;
    }
    else
    {
        printf("%s URI register failed, server NOT started", TAG);
        return ESP_ERR_INVALID_ARG;
    }
}

esp_err_t HTTPServer::start()
{
    printf("start1");
    static httpd_config_t config = HTTPD_DEFAULT_CONFIG();
    server = nullptr;
    printf("start2");
    auto result = httpd_start(&server, &config);
    printf("start3");
    if (result == ESP_OK)
    {   
        printf("%s server started", TAG);
    }
    else
    {
        printf("%s server start FAILURE", TAG);
    }
    return result;
}

esp_err_t HTTPServer::stop()
{
    auto result = ESP_OK;
    if(server)
    {
        result = httpd_stop(server);
        server = nullptr;
        printf("%s server stopped", TAG);
    }
    printf("%s server already been stoped", TAG);
    return result;
}

HTTPServer::~HTTPServer()
{
    ESP_ERROR_CHECK(stop());
}

esp_err_t HTTPServer::runProcedure()
{
    auto result = start();
    result += registerURI(uri_get);
    result += registerURI(uri_post);
    return result;
}