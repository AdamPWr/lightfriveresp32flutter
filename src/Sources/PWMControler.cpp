#include"../Headers/PWMControler.h"
#include"driver/ledc.h"
#include <algorithm>
#include<exception>
#include<string>


PWMControler::PWMControler(std::vector<uint16_t> p_channelsPinout) : 
channelsPinout(p_channelsPinout)
{
    timerConf = new ledc_timer_config_t();
    channelConf = new ledc_channel_config_t;
    for(auto i = 0; i< channelsPinout.size(); ++i)
    {
        currentChannelsDuty.emplace_back(0);
    }
}

PWMControler::~PWMControler()
{
    delete timerConf;
    delete channelConf;
    if(is_fade_func_installed)
    {
        uninstallFadeFunc();
    }
}

bool PWMControler::installFadeFunc()
{
    bool result = ledc_fade_func_install(0);
    if(result)
    {
        is_fade_func_installed = true;
    }
    return result;
}

void PWMControler::uninstallFadeFunc()
{
    if(is_fade_func_installed)
    {
        ledc_fade_func_uninstall();
    } 
}
    
int16_t PWMControler::getChannelNumber(u_int16_t pinNumber)
{
     std::vector<u_int16_t>::iterator itr = std::find(channelsPinout.begin(), channelsPinout.end(),pinNumber);

    if(itr == channelsPinout.end())
    {
        printf("There is no channel asociated with pin number: %d", pinNumber);
        return - 1;
    }
    return std::distance(channelsPinout.begin(), itr);
}

bool PWMControler::setPWMTimerConf()
{

    timerConf->freq_hz = 5000;
    timerConf->duty_resolution = LEDC_TIMER_13_BIT;
    timerConf->speed_mode = LEDC_HIGH_SPEED_MODE;
    timerConf->timer_num = LEDC_TIMER_0;
    auto result = ledc_timer_config(timerConf);

    return result == ESP_OK ? true : false;
}

bool PWMControler::setPWMChannelsConfig()
{
    bool result = true;
    for(int channelIndex = 0; channelIndex< channelsPinout.size();++channelIndex)
    {
        channelConf->channel = ledc_channel_t(channelIndex);
        channelConf->duty = currentChannelsDuty[channelIndex];
        channelConf->gpio_num = channelsPinout.at(channelIndex);
        channelConf->timer_sel = LEDC_TIMER_0;
        channelConf->speed_mode = LEDC_HIGH_SPEED_MODE;

        result &= ledc_channel_config(channelConf) == ESP_OK;
    }

    return result;
}

bool PWMControler::confPWM()
{
    return setPWMTimerConf() && setPWMChannelsConfig();
}

bool PWMControler::changeDutyCycleOnPin(uint16_t pinNumber, int16_t factor)
{
    int8_t channelNumber = getChannelNumber(pinNumber);
    if(channelNumber < 0)
    {
        return false;
    }

    auto temp_duty = currentChannelsDuty[channelNumber];
    if(currentChannelsDuty[channelNumber] + factor <= MAX_DUTY_RESOLUTION && currentChannelsDuty[channelNumber] + factor >= 0)
    {
        temp_duty += factor;
    }
    else{

        if(factor>0)
        {
            temp_duty = 0;
        }
        else{
            temp_duty = MAX_DUTY_RESOLUTION;
        }
    }
    bool result = true;
    result = ledc_set_duty(LEDC_HIGH_SPEED_MODE, ledc_channel_t(channelNumber), temp_duty) == ESP_OK;
    result &= (ledc_update_duty(LEDC_HIGH_SPEED_MODE, ledc_channel_t(channelNumber)) == ESP_OK);
    
    currentChannelsDuty[channelNumber] = result ? temp_duty : currentChannelsDuty[channelNumber];
    return result;
}

bool PWMControler::changeDutyCycleOnAllPins(int16_t factor)
{
    bool result = true;
    for(auto& pin :channelsPinout)
    {
       result &= changeDutyCycleOnPin(pin, factor);
    }
    return result;
}

bool PWMControler::setMaxDutyOnPin(uint16_t pinNumber)
{
    int8_t channelNumber = getChannelNumber(pinNumber);
    if(channelNumber < 0)
    {
        return false;
    }

    bool result;
    result = ledc_set_duty(LEDC_HIGH_SPEED_MODE, ledc_channel_t(channelNumber), MAX_DUTY_RESOLUTION) ==  ESP_OK;
    result = result && (ledc_update_duty(LEDC_HIGH_SPEED_MODE, ledc_channel_t(channelNumber)) == ESP_OK);
    if(result)
    {
        currentChannelsDuty[channelNumber] = MAX_DUTY_RESOLUTION;
    }
    return result;
}

bool PWMControler::setMaxDutyOnAllPins()
{
    bool result = true;
    for(auto& pin :channelsPinout)
    {
       result &= setMaxDutyOnPin(pin);
    }
    return result;
}

bool PWMControler::setZeroDutyOnPin(u_int16_t pinNumber)
{
    int8_t channelNumber = getChannelNumber(pinNumber);
    if(channelNumber < 0)
    {
        return false;
    }

    bool result;
    result = ledc_set_duty(LEDC_HIGH_SPEED_MODE, ledc_channel_t(22), 0) ==  ESP_OK;
    result = result && (ledc_update_duty(LEDC_HIGH_SPEED_MODE, ledc_channel_t(channelNumber))  ==  ESP_OK);
    
    if(result)
    {
        currentChannelsDuty[channelNumber] = 0;
    }
    return result;
}

bool PWMControler::setZeroDutyOnAllPins()
{
    bool result = true;
    for(auto& pin :channelsPinout)
    {
       result &= setZeroDutyOnPin(pin);
    }
    return result;
}

bool PWMControler::fadeWithTimeOnPin(uint32_t target_duty, uint32_t max_fade_time_ms, u_int16_t pinNumber)
{
    int8_t channelNumber = getChannelNumber(pinNumber);
    if(channelNumber < 0)
    {
        return false;
    }

    if(target_duty > MAX_DUTY_RESOLUTION)
    {
        return false;
    }
    bool result = true;
    if(!is_fade_func_installed)
    {
        result = installFadeFunc();
    }
    result = result && (ledc_set_fade_time_and_start(LEDC_HIGH_SPEED_MODE, ledc_channel_t(channelNumber),
                                                    target_duty, max_fade_time_ms, LEDC_FADE_NO_WAIT) ==  ESP_OK);
                                   
    return result;
}

bool PWMControler::fadeWithTimeOnAllPins(uint32_t target_duty, uint32_t max_fade_time_ms)
{
    bool result = true;
    for(auto& pin :channelsPinout)
    {
       result &= fadeWithTimeOnPin(target_duty, max_fade_time_ms, pin);
    }
    return result;
}

bool PWMControler::setDutyCycleOnPin(u_int8_t pinNumber, u_int8_t procentFactor)
{
    int8_t channelNumber = getChannelNumber(pinNumber);
    if(channelNumber < 0)
    {
        return false;
    }

    bool result;
    const auto duty = MAX_DUTY_RESOLUTION * procentFactor / 100;
    result = ledc_set_duty(LEDC_HIGH_SPEED_MODE, ledc_channel_t(channelNumber), duty) == ESP_OK;
    result &= (ledc_update_duty(LEDC_HIGH_SPEED_MODE, ledc_channel_t(channelNumber)) == ESP_OK); 

    if(result)
    {
        currentChannelsDuty[channelNumber] = duty;
    }
    
    return result;
}

bool PWMControler::setDutyCycleOnAllPins(u_int8_t procentFactor)
{
    bool result = true;
    for(auto& pin :channelsPinout)
    {
       result &= setDutyCycleOnPin(pin, procentFactor);
    }
    return result;
}

bool PWMControler::setFadeWithTimeByProcentOnPin(u_int8_t pinNumber, u_int8_t procentFactor, uint32_t max_fade_time_ms)
{
    if(procentFactor > 100)
    {
        printf("Bad argument in setFadeWithTimeByProcentOnPin(u_int8_t pinNumber, u_int8_t procentFactor, uint32_t max_fade_time_ms)");
        return false;
    }

    const auto duty = MAX_DUTY_RESOLUTION * procentFactor / 100;
    return fadeWithTimeOnPin(duty, max_fade_time_ms, pinNumber);
}

bool PWMControler::setFadeWithTimeByProcentOnAllPins(u_int8_t procentFactor, uint32_t max_fade_time_ms)
{
    if(procentFactor > 100)
    {
        printf("Bad argument in setFadeWithTimeByProcentOnPin(u_int8_t pinNumber, u_int8_t procentFactor, uint32_t max_fade_time_ms)");
        return false;
    }

    const auto duty = MAX_DUTY_RESOLUTION * procentFactor / 100;
    return fadeWithTimeOnAllPins(duty, max_fade_time_ms);
}
