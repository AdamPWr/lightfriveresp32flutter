import 'package:fab_circular_menu/fab_circular_menu.dart';
import 'package:flutter/material.dart';
import 'package:mobileappled/db/color_model.dart';
import 'package:mobileappled/db/db_helper.dart';
import 'package:mobileappled/providers/colors_transitions_provider.dart';
import 'package:mobileappled/widgets/app_bar.dart';
import 'package:mobileappled/widgets/menu_drawer.dart';
import 'package:mobileappled/widgets/transition_tile.dart';
import 'package:provider/provider.dart';

class TransitionsScreen extends StatefulWidget {

  late String appBarTitle = "";

  TransitionsScreen({required this.appBarTitle});

  @override
  _TransitionsScreenState createState() => _TransitionsScreenState();
}

class _TransitionsScreenState extends State<TransitionsScreen> {

  List<List<ColorModel>> transitions= [];
  late Future<List<List<ColorModel>>> transitionsFetched = _fetch();
  DBHelper dbHelper = DBHelper();

  Future<List<List<ColorModel>>> _fetch() async{
    await dbHelper.openDB();
    var transitions = await dbHelper.fetchTransitions();
    await dbHelper.closeDB();
    return transitions;
  }

  @override
  Widget build(BuildContext context) {


    final colorTransitionProvider = Provider.of<ColorsTransitionsProvider>(context, listen: true);
    print("TransitionsScreen build");
    //if(transitionsFetched.)
    return Scaffold(
      appBar: buildAppBar(widget.appBarTitle),
      drawer: menuDrawerWidget,

      body: Column(

        children: [
           Container(
            padding: const EdgeInsets.all(15.0),
              child: const Text("Your transitions", style: TextStyle(fontWeight: FontWeight.bold),),
          ),
          Expanded(
              child: Center(
                child: FutureBuilder(
                  future: transitionsFetched,//_fetch(),
                  builder: (context, AsyncSnapshot<List<List<ColorModel>>> snapshot)
                  {
                    print("Futurebuilder ${snapshot.data}");
                    if(snapshot.connectionState == ConnectionState.done)
                      {
                        print("Futurebuilder with data ${snapshot.data}");
                        List<List<ColorModel>> transitions = snapshot.data ?? [];
                        return ListView.separated(
                            itemCount: snapshot.data!.length,
                          separatorBuilder: (ctx,i){return const Divider();},
                          itemBuilder: (ctx,idx){
                              print("Row idx: $idx");
                              return TransitionTile(transition: transitions[idx], id: idx,);
                          },
                        );
                      }
                    else
                      {
                        return Container(height: 100,color: Colors.red,);
                      }
                  },
                ),
              ),
          )
        ],
      ),

      floatingActionButton: FabCircularMenu(
        ringColor: Colors.blueGrey,
        children: [
          IconButton(
              onPressed: (){
                Navigator.pushNamed(context, '/addTransitionPattern', arguments: {'name' : "Add Pattern"} );
          },
              icon: const Icon(Icons.add, color: Colors.black,)),
          IconButton(onPressed: (){}, icon: const Icon(Icons.home))
        ],
      ),
    );
  }
}
