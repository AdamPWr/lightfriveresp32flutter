import 'package:flutter/material.dart';
import 'package:mobileappled/buildWidgets/build_menu_tiles.dart';
import 'package:mobileappled/common/colors.dart';

final  menuDrawerWidget = MenuDrawer();

class MenuDrawer extends StatelessWidget {
  const MenuDrawer({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(

      child: Material(

        color: mainColor,
        child: ListView(
            children: buildMenuTiles(context)
        ),
      ),
    );
  }
}