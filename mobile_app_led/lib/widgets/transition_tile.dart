import 'package:clickable_list_wheel_view/clickable_list_wheel_widget.dart';
import 'package:flutter/material.dart';
import 'package:mobileappled/db/color_model.dart';
import 'package:mobileappled/providers/colors_transitions_provider.dart';
import 'package:provider/provider.dart';

class TransitionTile extends StatefulWidget {

  List<ColorModel> transition;
  final int id;

  TransitionTile({required this.transition, required this.id });

  @override
  _TransitionTileState createState() => _TransitionTileState();
}

class _TransitionTileState extends State<TransitionTile> {

  //ScrollController _scrolControler = ScrollController();

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: (){
        final colorTransitionProvider = Provider.of<ColorsTransitionsProvider>(context, listen: false);
        ColorsTransitionsProvider.selectedTransitionId = widget.id;
        colorTransitionProvider.refresh();
        print(widget.id);
        },
      child: Row(
        children: [
          Container(
            padding: const EdgeInsets.all(10.0),
            margin: const EdgeInsets.all(5.0),
            child: Text("${widget.id}"),
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: ColorsTransitionsProvider.selectedTransitionId == widget.id ? Colors.green : Colors.blue,
            ),
          ),
          Container(
            height: 80,
            width: 300,
//      color: widget.transition[0].color,
            child: ListView.separated(
                itemBuilder: (ctx, idx)
                {
                  print("color $idx ${widget.transition[idx].color.red}, ${widget.transition[idx].color.green} , ${widget.transition[idx].color.blue}, ${widget.transition[idx].color.opacity}   ");
                  return Container(width: 60, height: 30, color: widget.transition[idx].color,);
                },
                scrollDirection: Axis.horizontal,
                separatorBuilder: (ctx,i){return Container(width: 20,);},
                itemCount: widget.transition.length
            ),
          ),
        ],
      ),
    );

  }
}
