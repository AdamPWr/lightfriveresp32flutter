
import 'package:flutter/material.dart';
import 'package:mobileappled/common/custom_button.dart';
import 'package:flutter_colorpicker/flutter_colorpicker.dart';
import 'package:mobileappled/db/db_helper.dart';
import 'package:mobileappled/providers/colors_transitions_provider.dart';
import 'package:provider/provider.dart';

import 'app_bar.dart';
class AddColorsTransitionsScreen extends StatefulWidget {

  late String appBarTitle = "";

  AddColorsTransitionsScreen({required this.appBarTitle});

  @override
  _AddColorsTransitionsScreenState createState() => _AddColorsTransitionsScreenState();
}

class _AddColorsTransitionsScreenState extends State<AddColorsTransitionsScreen> {

  Color pickerColor = Color(0xff443a49);
  Color currentColor = Color(0xff443a49);
  DBHelper dbHelper = DBHelper();
  late int numberOfDifferentTransitionAlreadySavedInDB;


  @override
  void initState() {
    print("init state triggered in ad_transition_screen");
    super.initState();
    dbHelper.openDB().then
      (
            (value) {
          getNuberOfSavedTransitions().then
            (
                  (value) {
                numberOfDifferentTransitionAlreadySavedInDB = value;
              }
          );
        }
    );
  }


  @override
  void dispose() {
    super.dispose();
    print("dispose  triggered in ad_transition_scree");
    dbHelper.closeDB();
  }

  Future<int> getNuberOfSavedTransitions() async
  {
    int transitions = await dbHelper.getNumberOfDistinctTranstionsIdsFromDB();
    return transitions;
  }


  pickColour(BuildContext prevcontext)
  {
    showDialog(
        context: context,
        builder: (_){
          return AlertDialog(
            title: const Text("Pick a color"),
            content: SingleChildScrollView(
              child: ColorPicker(
                pickerColor: pickerColor,
                onColorChanged: (color){
                  pickerColor = color;
                },
              ),
            ),
            actions: <Widget>[
              ElevatedButton(
                child: const Text('Got it'),
                onPressed: () async{
                  int index =  numberOfDifferentTransitionAlreadySavedInDB;
                  setState((){
                    currentColor = pickerColor;
                    var colourList = Provider.of<ColorsTransitionsProvider>(prevcontext, listen: false);
                    colourList.addColorToComponedTransition(ColorsTransitionsProvider.convertColorToColorModel(currentColor,  index));
                  });
//                  DBHelper db = DBHelper();
//                  await db.openDB();
//                  db.addColorToDB(ColorsTransitionsProvider.convertColorToColorModel(currentColor,0));
//                  db.closeDB();
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        }
    );
  }

  @override
  Widget build(BuildContext context) {

    var transitionProvider = Provider.of<ColorsTransitionsProvider>(context,listen: true);


    return Scaffold(
      appBar: buildAppBar(widget.appBarTitle),

      body: Column(

        mainAxisAlignment: MainAxisAlignment.center,

        children: [
          Expanded(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 70),
              child: ListView.separated(
                separatorBuilder: (context, index) => const Divider(
                  color: Colors.black,
                ),
                itemCount: transitionProvider.componeTransitionList.length,
                itemBuilder: (context, index) => Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(color: transitionProvider.componeTransitionList.elementAt(index).color, height: 30,),
                ),
              ),
            ),
          ),
          Expanded(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const Spacer(),
                //CustomButton(text: "Pick", callback: pickColour,),
                ElevatedButton(
                  onPressed: (){
                    pickColour(context);
                  },
                  child: const Text("Pick a  color"),
                  style: ElevatedButton.styleFrom(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(12),
                    ),
                    minimumSize: const Size(100,50),
                  ),),
                const Spacer(),
                ElevatedButton(
                  onPressed: ()async {
                    //TODO pickColour(context);

                    await transitionProvider.addCurrentComponedTransitionToDB();
                    Navigator.of(context).pop();
                    print("Number of different transitions in db ${numberOfDifferentTransitionAlreadySavedInDB}");
                  },
                  child: const Text("Save"),
                  style: ElevatedButton.styleFrom(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(12),
                    ),
                    minimumSize: const Size(100,50),
                  ),),
                const Spacer(),
                ElevatedButton(
                  onPressed: ()async {
                    //TODO pickColour(context);

                    // Sprawdzic czy sie wszystko dobrze dodaje
                    print("Number of different transitions in db ${numberOfDifferentTransitionAlreadySavedInDB}");
                    await dbHelper.openDB();
                    dbHelper.fetchTransitions();
                  },
                  child: const Text("CHecjer"),
                  style: ElevatedButton.styleFrom(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(12),
                    ),
                    minimumSize: const Size(100,50),
                  ),),
                const Spacer(),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
