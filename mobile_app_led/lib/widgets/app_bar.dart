import 'package:flutter/material.dart';
import 'package:mobileappled/common/colors.dart';
import 'package:mobileappled/common/themes.dart';


AppBar buildAppBar(String tittle) {
  return AppBar(

  centerTitle: true,
    title: Text(tittle),
    //backgroundColor: mainColor,
    actions: [
      IconButton(onPressed: (){currentTheme.toogleTheme();}, icon: const Icon(Icons.brightness_6_outlined))
    ],
  );
}

