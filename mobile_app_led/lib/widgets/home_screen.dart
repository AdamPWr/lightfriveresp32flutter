import 'package:flutter/material.dart';
import 'package:mobileappled/common/http_client.dart';
import 'package:mobileappled/db/color_model.dart';
import 'package:mobileappled/widgets/menu_drawer.dart';


import 'app_bar.dart';

class Home extends StatefulWidget {

  late String appBarTitle = "";

  Home({required this.appBarTitle});

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {

  List<ColorModel> transition = [ColorModel(color: Colors.amber, transitionId: 1),ColorModel(color: Colors.blue, transitionId: 1),ColorModel(color: Colors.purple, transitionId: 1),];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(widget.appBarTitle),
      drawer: menuDrawerWidget,

      body: Center(

        child: ElevatedButton(
          onPressed: ()
          {
            HTTPClient client = HTTPClient();
            client.postTransitions(transition);
          },
          child: Text("Test"),
          style: ElevatedButton.styleFrom(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(12),
            ),
            minimumSize: const Size(100,50),
          ),
        ),
      ),
    );
  }

}
