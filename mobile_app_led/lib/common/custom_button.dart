import 'package:flutter/material.dart';

class CustomButton extends StatelessWidget {

  late final void Function() callback;
  late final String text;

  CustomButton({ required this.text, required this.callback});

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
        onPressed: callback,
        child: Text(text),
      style: ElevatedButton.styleFrom(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(12),
        ),
        minimumSize: const Size(100,50),
      ),
    );
  }
}
