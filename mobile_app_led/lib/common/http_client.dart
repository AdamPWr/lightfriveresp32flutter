import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:mobileappled/db/color_model.dart';

class HTTPClient
{
  final Uri postTransitionsURL = Uri.parse("http://192.168.4.1:80");
  //final Uri postTransitionsURLV2 = Uri(host: "192.168.0.1",path: "/setTransition");
  final mockUri = Uri.parse("https://44afdb77-d85e-4032-a374-1997bd6e0806.mock.pstmn.io");

  void postTransitions(List<ColorModel> transition) async
  {
    print("${postTransitionsURL.toString()}");

//      Map<String,String> transitionMap = {};
//      transition.forEach((element) {transitionMap.addAll(element.toSimpleMap());});
      var f = {"transitions" : transition};
      String json = jsonEncode(f);
      print(transition.length);
      print(json);

      var response = await http.post(postTransitionsURL,body: json).timeout(Duration(seconds: 5));

      if(response.statusCode >=200 && response.statusCode <300)
        {
          print("transition sended correctly");
          print(response.body);
        }
      else
        {
          print("Transition sending FAILED");
          print(response.statusCode);
        }
  }
}