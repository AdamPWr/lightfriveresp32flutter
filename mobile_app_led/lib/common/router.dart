import 'package:flutter/material.dart';
import 'package:mobileappled/widgets/add_transition_pattern_screen.dart';
import 'package:mobileappled/widgets/home_screen.dart';
import 'package:mobileappled/widgets/transitions_screen.dart';

class RouteGenerator {

  static Route<dynamic> generteRoute(RouteSettings settings){

    final args = settings.arguments;
    String appBarTittle =  settings.name == "/" ? "Module Driver" : 'Error';

    if( args is Map<String,String>)
      {
        appBarTittle = args['name'] ?? "Home";
      }



    switch(settings.name){
      case '/':
        {
          return MaterialPageRoute(builder: (_)=>Home(appBarTitle: appBarTittle));
        }
      case '/transitions':
        {
          return MaterialPageRoute(builder: (_)=>TransitionsScreen(appBarTitle: appBarTittle,));
        }
      case '/addTransitionPattern':
      {
        return MaterialPageRoute(builder: (_)=>AddColorsTransitionsScreen(appBarTitle: appBarTittle,));
      }
      default:
      {
        return MaterialPageRoute(builder: (_)=>Home(appBarTitle: appBarTittle,));
      }

//      case '/messgeTextScreen':
//        {
//          if(args is BuildContext)
//          {
//            return MaterialPageRoute(builder: (_)=>messageScreen(args));
//          }
//        }
//        break;
//      case '/blackListScreen':
//        {
//          if(args is BuildContext)
//          {
//            return MaterialPageRoute(builder: (_)=>BlackListScreen(args));
//          }
//        }
//        break;
//      case '/addNewMessagePatternScreen':
//        {
//          if(args is BuildContext)
//          {
//            return MaterialPageRoute(builder: (_)=>AddNewMessagePatternScreen(args));
//          }
//        }
//        break;
//      case '/bulkMsgScreen':
//        {
//          if(args is BuildContext)
//          {
//            return MaterialPageRoute(builder: (_)=>BulkMessageScreen(args));
//          }
//        }
//        break;

    }
  }

}