import 'package:flutter/material.dart';

CustomTheme currentTheme = CustomTheme();

class CustomTheme extends ChangeNotifier
{
  static bool _isDarkTheme = true;

    ThemeMode get currentTheme => _isDarkTheme ? ThemeMode.dark : ThemeMode.light;

    toogleTheme()
    {
      _isDarkTheme = !_isDarkTheme;
      notifyListeners();
    }

    static ThemeData get lightTheme
    {
      return  ThemeData(
          primaryColor: Colors.blue,
        backgroundColor: Colors.blue[100],
        scaffoldBackgroundColor: Colors.blue[200],
        textTheme: const TextTheme(
            headline1:  TextStyle(color: Colors.black),
          headline2:  TextStyle(color: Colors.black),
        )
      );
    }

    static ThemeData get darkTheme
    {
      return   ThemeData(
          primaryColor: Colors.blueGrey,
          backgroundColor: Colors.blueGrey[100],
          scaffoldBackgroundColor: Colors.blueGrey[200],
          appBarTheme: const AppBarTheme(color: Colors.blueGrey),
          floatingActionButtonTheme: const FloatingActionButtonThemeData(backgroundColor: Colors.blueGrey),
          textTheme:  const TextTheme(
            headline1:  TextStyle(color: Colors.black),
            headline2:  TextStyle(color: Colors.black),
            bodyText1:  TextStyle(color: Colors.black),
          )
      );
    }
}