import 'package:flutter/material.dart';

List<ListTile> buildMenuTiles(BuildContext context) {
  return <ListTile>[
    ListTile(

      title: const Text("Home"),
      leading: const Icon(Icons.home),
      onTap: ()
      {
        Navigator.pushReplacementNamed(context, '/', arguments: {'name' : "ModuleDriver"});
      },
    ),
    ListTile(

      title: const Text("Transitions"),
      leading: const Icon(Icons.arrow_forward_ios),
      onTap: ()
      {
          Navigator.pushReplacementNamed(context, '/transitions', arguments: {'name' : "Transitions"});
      },
    ),
    ListTile(

      title: const Text("LightSensors"),
      leading: const Icon(Icons.arrow_forward_ios),
      onTap: ()
      {

      },
    ),
  ];
}


