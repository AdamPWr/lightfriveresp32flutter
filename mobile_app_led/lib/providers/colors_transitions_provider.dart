
import 'package:flutter/material.dart';
import 'package:mobileappled/db/color_model.dart';
import 'package:mobileappled/db/db_helper.dart';

class ColorsTransitionsProvider extends ChangeNotifier
{

  List<ColorModel> componeTransitionList = [];
  static int selectedTransitionId = 0;

  void refresh()
  {
    notifyListeners();
    print("refresh ColorsTransitionsProvider");
  }

  addColorToComponedTransition(ColorModel color)
  {
    componeTransitionList.add(color);
    notifyListeners();
  }

  static convertColorToRGBList(Color color)
  {
    return [color.red, color.green, color.blue];
  }

  static convertColorToColorModel(Color color, int transitionID)
  {
    return ColorModel(color: color, transitionId: transitionID);
  }

  addCurrentComponedTransitionToDB() async
  {
      DBHelper dbHelper = DBHelper();
      await dbHelper.openDB();
      await dbHelper.insertTransition(componeTransitionList);
      await dbHelper.closeDB();
      componeTransitionList.clear();
      notifyListeners();
  }

  Future<List<List<ColorModel>>> fetchAllTransitions() async
  {
    DBHelper dbHelper = DBHelper();
    await dbHelper.openDB();
    var transitions = await dbHelper.fetchTransitions();
    await dbHelper.closeDB();
    notifyListeners();
    return transitions;
  }
}