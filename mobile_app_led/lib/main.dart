import 'package:flutter/material.dart';
import 'package:mobileappled/providers/colors_transitions_provider.dart';
import 'package:mobileappled/widgets/home_screen.dart';
import 'package:provider/provider.dart';

import 'common/router.dart';
import 'common/themes.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {

  @override
  void initState() {
    super.initState();
    currentTheme.addListener(() {
      setState(() {

      });
    });
  }
  @override
  Widget build(BuildContext context) {

    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) => ColorsTransitionsProvider()),
      ],
      child: MaterialApp(
        title: 'Module driver',
        debugShowCheckedModeBanner: false,
        theme: CustomTheme.lightTheme,
        darkTheme: CustomTheme.darkTheme,
        themeMode: currentTheme.currentTheme,
        initialRoute: '/',
        onGenerateRoute: RouteGenerator.generteRoute,
      ),
    );
  }


}
