

import 'package:flutter/material.dart';

class ColorModel
{
  late Color color;
  int transitionId;

  ColorModel({required this.color, required this.transitionId});

  Map<String, dynamic> toMap()
  {
    return {
      "transitionID" : transitionId,
      "r" : color.red,
      "g" : color.green,
      "b" : color.blue
    };
  }

  Map<String, String> toJson()
  {
    return {
      "r" : color.red.toString(),
      "g" : color.green.toString(),
      "b" : color.blue.toString()
    };
  }

  static ColorModelfromRGBRepresentation(int r, int g, int b, int transitionId)
  {
      return ColorModel(color: Color.fromRGBO(r, g, b, 1), transitionId: transitionId);
  }

  @override
  String toString() {
    // TODO: implement toString
    return "${color.toString()} transitionId: $transitionId";
  }
}