import 'package:flutter/material.dart';
import 'package:mobileappled/db/color_model.dart';
import 'package:sqflite/sqflite.dart';
import 'package:collection/collection.dart';

// TODO  DB is not closed at all
class DBHelper
{

  late Database db;// =  openDB();
  static const String dbName = 'ledDB';
  static const String colorssTable = 't_colors';


  Future openDB() async
  {
    print("DB opened");
    db =  await openDatabase(
        dbName,
        version: 1,
        onCreate: (database, version){
        // creating table for transitions
          database.execute('CREATE TABLE $colorssTable (id INTEGER PRIMARY KEY, transitionID INTEGER, r INTEGER, g INTEGER, b INTEGER)');
        });
  }

  closeDB() async
  {
    print("db closed");
    await db.close();
  }

  addColorToDB(ColorModel color) async
  {
    if(!db.isOpen)
      {
        throw "DB is nod opened before adding element";
      }
    print("inserted");
      await db.insert(colorssTable, color.toMap());
  }

  Future<List<ColorModel>> fetchAllColorsFromDb() async
  {
    print("dupafetch");
    if(!db.isOpen)
    {
      print("DB is not opened");
      throw "DB is nod opened before adding element";
    }
    print("dupa3");
    final List<Map<String, dynamic>> colorsMaps = await db.query(colorssTable);
    print("dupa4");
    return List.generate(colorsMaps.length, (i){
      final int  r = colorsMaps[i]['r'];
      final int g = colorsMaps[i]['g'];
      final int b = colorsMaps[i]['b'];
      final int transitionId = colorsMaps[i]['transitionID'];
      return ColorModel.ColorModelfromRGBRepresentation(r, g, b, transitionId);
    });
  }

  clearColorsTable() async
  {
    if(!db.isOpen)
    {
      throw "DB is nod opened before adding element";
    }
    await db.rawDelete("DELETE * FROM $colorssTable");
  }

  Future<int> getNumberOfDistinctTranstionsIdsFromDB() async
  {
    if(!db.isOpen)
    {
      throw "DB is nod opened before adding element";
    }

    var ids = await db.rawQuery("SELECT DISTINCT transitionID FROM $colorssTable;");
    print("distinct ids: $ids");
    return ids.length;

  }

  insertTransition(List<ColorModel> transition) async
  {
    if(!db.isOpen)
    {
      throw "DB is nod opened before adding element";
    }
    List<ColorModel> transitionModel = transition.map((e){return ColorModel(color: e.color, transitionId: e.transitionId);}).toList();

    for(var color in transitionModel)
      {
        await db.insert(colorssTable, color.toMap());
      }

  }
  Future<List<List<ColorModel>>> fetchTransitions() async{
    print("dupa");
    List<ColorModel> allColors = await fetchAllColorsFromDb();
    print("dupa2");
    Map<int, List<ColorModel>> colorsById = groupBy<ColorModel,int>(allColors, (color) => color.transitionId);
    List<List<ColorModel>> transition;
    transition = colorsById.values.toList();
    print("transitions fetched :${transition}");
    return transition;
  }
  
}